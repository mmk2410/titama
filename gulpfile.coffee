gulp = require 'gulp'
coffee = require 'gulp-coffee'

gulp.task 'coffee', ->
  gulp.src './res/js/*.coffee'
    .pipe coffee()
    .pipe gulp.dest './res/js/'

gulp.task 'default', ->
  gulp.watch './res/js/*.coffee', ['coffee']
