# TiTaMa

A time table manager with a beautiful material design.

## How does it work?

TiTaMa uses a MySQL database to store the timetable. For web interface PHP is used.

## What are the features?

 - Single page user interface
 - Responsive design

## HHVM

This page works also with HHVM

## Contributing

1. Fork it
2. Create a feature branch with a meaningful name (`git checkout -b my-new-feature`)
3. Add yourself to the CONTRIBUTORS file
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to your branch (`git push origin my-new-feature`)
6. Create a new pull request
