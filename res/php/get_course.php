<?php
if (!isset($_GET['id'])) {
    echo "No id given!";
    return;
}
$id = $_GET['id'];
include './config.php';
$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbname);
if ($conn->error) {
    die("Failed to connect to the database: " . $conn->error);
}
$sql = "SELECT * FROM Titama WHERE id=$id";
$res = $conn->query($sql);
if ($res->num_rows > 0) {
    $course = $res->fetch_assoc();
    echo json_encode($course);
}
include './closedb.php';
?>
