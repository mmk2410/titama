(function() {
  var change_course;

  $('#remove').click(function() {
    return window.location = "./";
  });

  change_course = function(id) {
    var httpRequest;
    httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
      var res;
      if (httpRequest.readyState === 4 && httpRequest.status === 200) {
        res = httpRequest.responseText;
        if (!res) {
          if ($('#change_id').val()) {
            $('#noid').html("<br>Id not available.");
          } else {
            $('#noid').html("");
          }
          $('#change_name').val("");
          $('#change_kind').val("");
          $('#change_room').val("");
          $('#change_day').val("");
          $('#change_time').val("");
          return $('#change_prof').val("");
        } else {
          res = JSON.parse(res);
          $('#noid').html("");
          $('#change_name').val(res.name);
          $('#change_kind').val(res.kind);
          $('#change_room').val(res.room);
          $('#change_day').val(res.day);
          $('#change_time').val(res.time);
          return $('#change_prof').val(res.prof);
        }
      }
    };
    httpRequest.open("GET", "./res/php/get_course.php?id=" + id, true);
    return httpRequest.send();
  };

  $('#change_id').keyup(function() {
    var arg;
    arg = $('#change_id').val();
    return change_course(arg);
  });

}).call(this);
