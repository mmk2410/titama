# CoffeeScript for TiTaMa
$('#remove').click ->
  window.location = "./"

change_course = (id) ->
  httpRequest = new XMLHttpRequest()
  httpRequest.onreadystatechange = ->
    if httpRequest.readyState == 4 && httpRequest.status == 200
      res = httpRequest.responseText
      if not res
        if $('#change_id').val()
          $('#noid').html("<br>Id not available.")
        else
          $('#noid').html("")
        $('#change_name').val("")
        $('#change_kind').val("")
        $('#change_room').val("")
        $('#change_day').val("")
        $('#change_time').val("")
        $('#change_prof').val("")
      else
        res = JSON.parse(res)
        $('#noid').html("")
        $('#change_name').val(res.name)
        $('#change_kind').val(res.kind)
        $('#change_room').val(res.room)
        $('#change_day').val(res.day)
        $('#change_time').val(res.time)
        $('#change_prof').val(res.prof)
  httpRequest.open "GET", "./res/php/get_course.php?id=" + id, true
  httpRequest.send()

$('#change_id').keyup ->
  arg = $('#change_id').val()
  change_course(arg)
