<!DOCTYPE html>
<!--
    TiTaMa - A TImeTAbleMAnager for the web

    MIT LICENSE

    2015 (c) Marcel Kapfer (mmk2410) <marcelmichaelkapfer@yahoo.co.nz>
-->
<?php
function getFullDay($day) {
    if ($day == "Mon") {
        return "Monday";
    } else if ($day == "Tue") {
        return "Tuesday";
    } else if ($day == "Wed") {
        return "Wednesday";
    } else if ($day == "Thur") {
        return "Thursday";
    } else if ($day == "Fri") {
        return "Friday";
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>TiTaMa</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
        <meta name="description" content="A web time table manager">

        <meta name="theme-color" content="#3f51b5">

        <link rel="manifest" href="manifest.json">

        <meta name="msapplication-TileColor" content="#3f51b5">

        <meta name="mobile-web-app-capable" content="yes">
        <meta name="application-name" content="TiTaMa">
        <meta rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="TiTaMa">
        <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

        <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144.png">

        <link href='//fonts.googleapis.com/css?family=Roboto:400,600,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./res/titama.css" />
    </head>
    <body>
        <div class="header">
            <span class="title">TiTaMa</span>
        </div>
<?php
include './res/php/config.php';
if (isset($_GET['add'])) {
    $conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbname);
    $name = $_POST['name'];
    $kind = $_POST['kind'];
    $room = $_POST['room'];
    $day = $_POST['day'];
    $time = $_POST['time'];
    $prof = $_POST['prof'];
    if ($conn->error) {
?>
            <section class="card">
                <div class="headline">Error</div>
<?php
        die("Failed to connect to the database: " . $conn->error);
?>
                </section>
<?php
    }
    $sql = "INSERT INTO Titama (name, kind, room, day, time, prof)
        VALUES('$name', '$kind', '$room', '$day', '$time', '$prof')";
    if ($conn->query($sql) === TRUE) {
?>
            <section class="card">
                <div class="headline">Success</div>
<?php
        echo "The new course was created successfully.";
?>
                    <br>
                    <br>
                    <span id="remove" class="button">OK</span>
                </section>
<?php
    } else {
?>
                <section class="card">
                    <div class="headline">Error</div>
<?php
        echo "Error:" . $sql . "<br>" . $conn->error;
?>
                    <br>
                    <br>
                    <span id="remove" class="button">OK</span>
                </section>
<?php
    }
}

if (isset($_GET['change'])) {
    $conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbname);
    $userid = $_POST['userid'];
    $name = $_POST['name'];
    $kind = $_POST['kind'];
    $room = $_POST['room'];
    $day = $_POST['day'];
    $time = $_POST['time'];
    $prof = $_POST['prof'];
    if ($conn->error) {
?>
                    <section class="card">
                        <div class="headline">Error</div>
<?php
        die("Failed to connect to the database: " . $conn->error);
?>
                    <br>
                    <br>
                    <span id="remove" class="button">OK</span>
                    </section>
<?php
    }
    $sql = "UPDATE Titama SET name='$name',kind='$kind',room='$room',day='$day',time='$time',prof='$prof' WHERE id=$userid";
    if ($conn->query($sql)) {
?>
                    <section class="card">
                        <div class="headline">Success</div>
<?php
        echo "Course successfully updated";
?>
                    <br>
                    <br>
                    <span id="remove" class="button">OK</span>
                    </section>
<?php
    } else {
?>
                    <section class="card">
                        <div class="headline">Error</div>
<?php
        echo "Some error occured. SQL: " . $sql . "<br> ERROR: " . $conn->error;
?>
                    <br>
                    <br>
                    <span id="remove" class="button">OK</span>
                    </section>
<?php
    }
}

if (isset($_GET['del'])) {
    $userid = $_POST['userid'];
    $conn = new mysqli($conn, $dbuser, $dbpassword, $dbname);
    if ($conn->error) {
?>
                <section class="card">
                    <div class="headline">Error</div>
<?php
        die("Failed to connect to the database: " . $conn->error);
?>
                    <br>
                    <br>
                    <span id="remove" class="button">OK</span>
                </section>
<?php
    }
    $sql = "DELETE FROM Titama WHERE id=$userid";
    $results = $conn->query($sql);
    if ($conn->query($sql)) {
?>
                <section class="card">
                    <div class="headline">Success</div>
<?php
        echo "Course successfully deleted";
?>
                    <br>
                    <br>
                    <span id="remove" class="button">OK</span>
                </section>
<?php
    } else {
?>
                <section class="card">
                    <div class="headline">Error</div>
<?php
        echo "Some error occured. SQL: " . $sql . "<br> ERROR: " . $conn->error;
?>
                    <br>
                    <br>
                    <span id="remove" class="button">OK</span>
                </section>
<?php
    }
}

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbname);
if ($conn->error) {
?>
                    <section class="card">
                        <div class="headline">Error</div>
<?php
    die("Failed to connect to the database: " . $conn->error);
?>
                    <br>
                    <br>
                    <span id="remove" class="button">OK</span>
                    </section>
<?php
}
$days = array("Mon", "Tue", "Wed", "Thur", "Fri");
foreach ($days as $day) {
    $sql = "SELECT * FROM Titama WHERE day='$day'";
    $res = $conn->query($sql);
    if ($res->num_rows > 0) {
?>
                    <section class="card">
                    <div class="headline"><?php echo getFullDay($day) ?></div>
<?php
        $courses = array();
        $result = $conn->query($sql);
        while ($row = $result->fetch_assoc()) {
            $courses[] = $row;
        }
        usort($courses, function($a, $b) {
            return $a["time"] - $b["time"];
        });
        foreach ($courses as $course) {
?>
<div class="course">
    <div class="c_name" id="c_name_<?php echo $course["id"] ?>"><?php echo $course["name"]; ?></div>
    <div class="c_time"><?php echo $course["time"]; ?>:00</div>
    <div class="c_id">Id: <?php echo $course["id"]; ?></div>
    <div class="c_kind">Kind: <?php echo $course["kind"]; ?></div>
    <div class="c_place">Place: <?php echo $course["room"]; ?></div>
    <div class="c_prof">Prof: <?php echo $course["prof"]; ?></div>
</div>
<?php
        }
    }
?>
                    </section>
<?php
}
include './res/php/closedb.php';
?>
            <section class="card">
                <div class="headline">Add new course</div>
                <form action="?add=1" method="post">
                    <p>Name:</p><input name="name" class="itextfield"/>
                    <p>Kind:</p><input name="kind" class="itextfield"/>
                    <p>Room:</p><input name="room" class="itextfield"/>
                    <p>Day:</p><input name="day" class="itextfield"/>
                    <p>Starts at:</p><input name="time" class="itextfield"/>
                    <p>Prof:</p><input name="prof" class="itextfield"/>
                    <br>
                    <br>
                    <input type="submit" class="button" value="ADD"/>
                </form>
            </section>
            <section class="card">
                <div class="headline">Change course</div>
                <form action="?change=1" method="post">
                    <p>Course ID:</p><input name="userid" id="change_id" class="itextfield"/>
                    <div id="noid"></div>
                    <p>Name:</p><input name="name" id="change_name" class="itextfield"/>
                    <p>Kind:</p><input name="kind" id="change_kind" class="itextfield"/>
                    <p>Room:</p><input name="room" id="change_room" class="itextfield"/>
                    <p>Day:</p><input name="day" id="change_day" class="itextfield"/>
                    <p>Starts at:</p><input name="time" id="change_time" class="itextfield"/>
                    <p>Prof:</p><input name="prof" id="change_prof" class="itextfield"/>
                    <br>
                    <br>
                    <input type="submit" class="button" value="CHANGE" />
                </form>
            </section>
            <section class="card">
                <div class="headline">Delete course</div>
                <form action="?del=1" method="post">
                    <p>Course ID:</p><input name="userid" class="itextfield"/>
                    <br>
                    <br>
                    <input type="submit" class="button" value="DELETE" />
                </form>
            </section>
        </section>
        <div class="footer">Marcel Kapfer (mmk2410) - 2015-2016</div>
    <script src="./res/jquery-2.1.4.min.js"></script>
    <script src="./res/js/titama.js"></script>
</body>
</html>
