#!/bin/bash
# TiTaMa - A web time table manager
# Shell script for initialiying the whole thing
# 2015 (c) Marcel Kapfer (mmk2410) <marcelmichaelkapfer@yahoo.co.nz>
# MIT License
echo
echo "Welcome to TiTaMa."
echo
echo "To initialize TiTaMa please enter the following values."
echo
echo -n "Database Host: "
read -r host
echo -n "Database User: "
read -r user
echo -n "Database Name: "
read -r db
echo -n "Database password: "
read -r password
echo
config=$(
cat << EOF
<?php
// TiTaMa - A web time table manager
// 2015 \(c\) Marcel Kapfer \(mmk2410\) <marcelmichaelkapfer@yahoo.co.nz>
// MIT License
\$dbhost = "$host";
\$dbuser = "$user";
\$dbname = "$db";
\$dbpassword = "$password";
EOF
)
data=$(
cat << EOF
Database Host:      $host
Database User:      $user
Database Name:      $db
Database Password:  $password
EOF
)
if [[ -s ./res/php/config.php ]]; then
    echo -n "File already exists. Do you want to override it? (y/n) "
    read -r option
    if [[ "$option" == "y" ]]; then
        echo
        echo "$data"
        echo
        echo -n "Is the data right? (y/n) "
        read -r correct
        if [[ "$correct" == "y" ]]; then
            echo "$config" > ./res/php/config.php
        else
            echo "Aborting"
            echo
            exit
        fi
    else
        echo "Exiting without overriding the existing file."
        echo
        exit
    fi
else
    echo
    echo "$data"
    echo
    echo -n "Is the data right? (y/n) "
    read -r correct
    if [[ "$correct" == "y" ]]; then
        echo "$config" > ./res/php/config.php
    else
        echo "Aborting"
        echo
        exit
    fi
fi
add_info=$(
cat << EOF
Additional Information:

To use TiTaMa you have to initialize the MySQL database of your system. Please follow the instruction of your operating systems.

After that you should create a user. You can do so in the MySQL CLI with this commands:

CREATE USER '$user'@'$host' IDENTIFIED BY '$password';

GRANT ALL PRIVILEGES ON *.* TO '$user'@'$host'
WITH GRANT OPTION;

Then create the database:

CREATE DATABASE $db

and last create the table:

CREATE TABLE $db.Titama( id INT(7) UNSIGNED AUTO_INCREMENT PRIMARY KEY, name TEXT, kind TEXT, room TEXT, day TEXT, time TEXT, prof TEXT);

EOF
)
echo
echo "$add_info"
echo
echo "Have fun using TiTaMa!"
echo
